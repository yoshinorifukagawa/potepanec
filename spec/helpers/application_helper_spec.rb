require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "#full_title" do
    subject { full_title(page_title: page_title) }

    context '引数が文字列の場合' do
      let(:page_title) { 'test' }

      it 'ページタイトルが文字列を含むこと' do
        expect(subject).to eq 'test - BIGBAG Store'
      end
    end

    context '引数がnilの場合' do
      let(:page_title) { nil }

      it 'BASE_TITLEが表示されること' do
        expect(full_title(page_title: nil)).to eq 'BIGBAG Store'
      end
    end

    context '引数が空の場合' do
      let(:page_title) { '' }

      it 'BASE_TITLEが表示されること' do
        expect(full_title(page_title: '')).to eq 'BIGBAG Store'
      end
    end
  end
end
