require 'rails_helper'

RSpec.describe 'Potepan::Categories', type: :request do
  describe 'GET #show' do
    let!(:product) { create(:product, taxons: [taxon]) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:taxonomy) { create(:taxonomy) }
    let(:image) { create(:image) }
    let!(:product_1) { create(:product, taxons: [taxon_1]) }
    let(:taxon_1) { create(:taxon, taxonomy: taxonomy) }

    before do
      product.images << image
      get potepan_category_path(taxon.id)
    end

    it '200レスポンスが返ってくること' do
      expect(response).to have_http_status 200
    end

    it '商品名が含まれていること' do
      expect(response.body).to include product.name
    end

    it '商品価格が含まれていること' do
      expect(response.body).to include product.display_price.to_s
    end

    it 'taxonが含まれていること' do
      expect(response.body).to include taxon.name
    end

    it 'カテゴリーの商品数が含まれていること' do
      expect(response.body).to include taxon.products.count.to_s
    end

    it "商品画像が含まれていること" do
      expect(response.body).to include product.images.first.attachment(:product)
    end

    it "選択したtaxonに紐付かない商品は表示されないこと" do
      expect(response.body).not_to include product_1.name
    end
  end
end
