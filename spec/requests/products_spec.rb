require 'rails_helper'

RSpec.describe 'Potepan::Products', type: :request do
  describe 'GET #show' do
    let(:product) { create(:product, taxons: [taxon]) }
    let(:taxon) { create(:taxon) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it 'レスポンスが正常であること' do
      expect(response).to be_successful
    end

    it '200レスポンスが返ってくること' do
      expect(response).to have_http_status '200'
    end

    it '商品名が含まれていること' do
      expect(response.body).to include product.name
    end

    it '商品説明が含まれていること' do
      expect(response.body).to include product.description
    end

    it '商品価格が含まれていること' do
      expect(response.body).to include product.display_price.to_s
    end

    it '関連商品が4つ含まれていること' do
      expect(response.body).to include related_products[0].name
      expect(response.body).to include related_products[1].name
      expect(response.body).to include related_products[2].name
      expect(response.body).to include related_products[3].name
    end
  end
end
