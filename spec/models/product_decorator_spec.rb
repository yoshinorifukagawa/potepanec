require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  describe 'related_products' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }
    let(:related_product) { create(:product, taxons: [taxon]) }
    let(:taxon_1) { create(:taxon, taxonomy: taxonomy) }
    let(:unrelated_product) { create(:product, taxons: [taxon_1]) }

    it '関連商品が含まれ、関連しない商品は含まれないこと' do
      expect(product.related_products).to match_array [related_product]
    end
  end
end
