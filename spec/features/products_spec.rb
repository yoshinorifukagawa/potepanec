require 'rails_helper'

RSpec.feature "Potepan::Products", type: :feature do
  let(:product) { create(:product, taxons: [taxon]) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
  let(:taxonomy) { create(:taxonomy) }
  let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

  background do
    visit potepan_product_path(product.id)
  end

  scenario 'タイトルにproduct.nameが含まれること' do
    expect(page).to have_title product.name
  end

  scenario 'ヘッダーに商品名とHomeへのリンクがあること' do
    within '.pageHeader' do
      expect(page).to have_selector 'h2', text: product.name
      expect(page).to have_selector 'li', text: product.name
      expect(page).to have_link 'Home'
      click_on 'Home'
      expect(current_path).to eq potepan_index_path
    end
  end

  scenario '詳細ページにリンクとテキストがあること' do
    within '.singleProduct' do
      expect(page).to have_selector 'h2', text: product.name
      expect(page).to have_selector 'h3', text: product.display_price.to_s
      expect(page).to have_selector 'p', text: product.description
      expect(page).to have_link '一覧ページへ戻る'
      expect(page).to have_link 'カートへ入れる'
    end
  end

  scenario '関連商品が正しく表示されていること' do
    within '.productsContent' do
      related_products.each do |related_product|
        expect(page).to have_selector 'h5', text: related_product.name
        expect(page).to have_selector 'h3', text: related_product.display_price.to_s
        expect(page).to have_link related_product.name
        expect(page).to have_link related_product.display_price.to_s
        click_on related_product.name
        expect(current_path).to eq potepan_product_path(related_product.id)
      end
    end
  end
end
